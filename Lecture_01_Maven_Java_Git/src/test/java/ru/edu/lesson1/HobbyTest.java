package ru.edu.lesson1;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HobbyTest extends TestCase {

    private Hobby hobby;

    public void setUp() throws Exception {
       hobby= new Hobby("","");
    }
@Test
    public void testGetId() {
    assertNotNull(hobby.getId());
    assertEquals("",hobby.getId());}
@Test
    public void testTestGetName() {
    assertNotNull(hobby.getName());}
@Test
    public void testGetDescription() {
    assertNotNull(hobby.getDescription());}
@Test
    public void testTestToString() {
    assertNotNull(hobby.toString());}
}