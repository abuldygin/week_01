package ru.edu.lesson1;

public class Hobby {
    private final String id;
    private final String name;
    private final String description;

    public Hobby(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Hobby(String id, String name) {
        this.description="По умолчанию";
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
