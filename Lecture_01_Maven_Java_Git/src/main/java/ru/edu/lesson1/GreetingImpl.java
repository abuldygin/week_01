package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class GreetingImpl implements IGreeting{
    private String firstName="Антон";

    private String secondName ="Викторович";

    private String lastName = "Булдыгин";

    private List<Hobby> hobbies = new ArrayList<>();


    private String bitbucketUrl= "https://bitbucket.org/abuldygin/lecture1_git/src/master/";

    private String phone ="+7 903 112 38 49";

    private String courseExpectation =" Стать профессиональным Java разработчиком";

    private String educationInfo = " Высшее. МИСиС. Аспирантура";

    public GreetingImpl() {
        this.setHobbies();
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getSecondName() {
        return secondName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    public void setHobbies() {
        hobbies.add(new Hobby("1","bike", "bike riding"));
        hobbies.add(new Hobby("2","java", "java developing"));
        hobbies.add(new Hobby("3","Traveling", "Visiting different countries"));
        hobbies.add(new Hobby("4","Skating"));


    }

    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getCourseExpectation() {
        return courseExpectation;
    }

    @Override
    public String getEducationInfo() {
        return educationInfo;
    }

    @Override
    public String toString() {
        return "GreetingImpl{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", hobbies=" + this.getHobbies().toString() +
                ", bitbucketUrl='" + bitbucketUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", courseExpectation='" + courseExpectation + '\'' +
                ", educationInfo='" + educationInfo + '\'' +
                '}';
    }
}
